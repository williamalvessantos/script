#!/bin/bash

## Change to the dir where the script is located and
# get the full name into a variable
cd "$(dirname $0)"
WORK_DIR="$(pwd)"

# ------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------

function redirect(){
   echo "location ${1}  {"
   echo "rewrite  ^${1}$  ${2}?   permanent;"
   echo "try_files $uri $uri/ @handler;}"
}

# ------------------------------------------------------------------------
# Start                                                                                                                                                             # ------------------------------------------------------------------------

cat ${1} | sed 's@.html@@1' | awk '{print substr($1, 25), $3}' | while read values; do                                                                              
   redirect ${values}
done   

# ------------------------------------------------------------------------
# End
# ------------------------------------------------------------------------

exit 0
